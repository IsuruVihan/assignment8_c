/*

	Isuru Harischandra
	2019/IS/033

*/

#include <stdio.h>

// maximum number of students
#define MAX 100

// student 
struct student {
	char fname[20];
	char subject[20];
	int marks;
};

// student array
struct student s[MAX];

int main() {
	// max => user defined maximum count of students
	// num => current number of student
	int max, num;
	
	// getting the student count
	while (1) {
		puts("Enter the numebr of students that you wish to submit (Max = 100): ");
		scanf("%d", &max);
		
		if (max <= MAX && max > 0) {
			break;
		} else {
			puts("Invalid input! Student count should be a value betwen 0 & 100\n");
		}
	}

	// get student details
	for (num = 1; num <= max; num++) {
		printf("-------------------------------\nEnter details of student %d => \n\n", num);
		printf("First name: \n");
		scanf("%s", s[num - 1].fname);
		printf("Subject: \n");
		scanf("%s", s[num - 1].subject);
		printf("Marks: ");
		scanf("%d", &s[num - 1].marks);
	}

	printf("\n\n\n----------\nSummary...\n----------\n\n\n");

	// print student details
	for (num = 1; num <= max; num++) {
		printf("Student %d =>\n", num);
		printf("First name\t: %s\n", s[num - 1].fname);
		printf("Subject\t\t: %s\n", s[num - 1].subject);
		printf("Marks\t\t: %d\n-------------------------------\n\n", s[num - 1].marks);
	}

	return 0;
}